ARG ZAP_VERSION=w2019-09-24
FROM owasp/zap2docker-weekly:$ZAP_VERSION

ARG FIREFOX_VERSION=59.0.2
ARG GECKODRIVER_VERSION=0.19.1

USER root

WORKDIR /output

# Install a Firefox version that is compatible with Selenium
RUN apt-get -y remove firefox && \
      cd /opt && \
      wget http://ftp.mozilla.org/pub/firefox/releases/$FIREFOX_VERSION/linux-x86_64/en-US/firefox-$FIREFOX_VERSION.tar.bz2 && \
      tar -xvjf firefox-$FIREFOX_VERSION.tar.bz2 && \
      rm firefox-$FIREFOX_VERSION.tar.bz2 && \
      ln -s /opt/firefox/firefox /usr/bin/firefox

# Install Selenium and Mozilla webdriver
RUN cd /opt && \
      wget https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
      tar -xvzf geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
      rm geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
      chmod +x geckodriver && \
      ln -s /opt/geckodriver /usr/bin/geckodriver && \
      export PATH=$PATH:/usr/bin/geckodriver

# Install python dependencies
ADD requirements.txt /dast-requirements.txt
RUN pip install --no-cache -r /dast-requirements.txt

# Add custom ZAP hook that handles authentication
COPY src/hooks.py /home/zap/.zap_hooks.py

# Use a custom log file for ZAP
COPY config/zap-log4j.properties /root/.ZAP_D/log4j.properties

# Wrap zap-baseline.py and zap-full-scan.py to support cli parameters for authentication
RUN mv /zap/zap-baseline.py /zap/zap_baseline_original.py && \
    mv /zap/zap-full-scan.py /zap/zap_full_scan_original.py

COPY --chown=zap src/* /zap/

ADD analyze /analyze

ENTRYPOINT []
CMD ["/analyze"]
