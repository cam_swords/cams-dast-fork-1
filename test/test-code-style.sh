#!/bin/sh
# Test code style:  ./test/test-code-style.sh
set -e

TEST_DIR="$(dirname "$(realpath "$0")")"

# Lint Python
find "$TEST_DIR/.." -name "*.py" -print0 | xargs -0 pycodestyle --show-source --config="$TEST_DIR/../.pep8"

# Lint Shell scripts
shellcheck ./analyze
find "$TEST_DIR/.." -name "*.sh" -print0 | xargs -0 shellcheck
