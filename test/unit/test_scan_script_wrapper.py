from unittest.mock import MagicMock
from unittest import TestCase
from scan_script_wrapper import ScanScriptWrapper
from mock_config import ToConfig


class ToObject:
    def __init__(self, **key_values):
        self.__dict__.update(key_values)


class ScanScriptWrapperTest(TestCase):
    def setUp(self):
        self.original_main = ToObject(main=MagicMock())
        self.system = MagicMock()
        self.logger = MagicMock()
        self.config = ToConfig(target="http://website")
        self.site_check = ToObject(is_available=MagicMock(return_value=True),
                                   unavailable_reason=MagicMock(return_value=None),
                                   is_safe_to_scan=MagicMock(return_value=(True, None)))
        self.target_website = ToObject(address=MagicMock(return_value="http://website"),
                                       is_configured=MagicMock(return_value=True),
                                       check_site_is_available=MagicMock(return_value=self.site_check))

    def test_should_throw_when_target_not_set(self):
        target_website = ToObject(address=MagicMock(return_value="http://website"),
                                  is_configured=MagicMock(return_value=False),
                                  check_site_is_available=MagicMock(return_value=self.site_check))
        ScanScriptWrapper(self.original_main, self.config, self.logger, target_website, self.system).run()

        self.system.exit.assert_called_once()
        self.original_main.main.assert_not_called()

    def test_should_exit_when_target_not_safe_to_scan(self):
        site_check = ToObject(is_available=MagicMock(return_value=True),
                              unavailable_reason=MagicMock(return_value=None),
                              is_safe_to_scan=MagicMock(return_value=(False, 'a good reason')))

        target_website = ToObject(address=MagicMock(return_value="http://website"),
                                  is_configured=MagicMock(return_value=True),
                                  check_site_is_available=MagicMock(return_value=site_check))
        ScanScriptWrapper(self.original_main, self.config, self.logger, target_website, self.system).run()

        self.system.exit.assert_called_once()
        self.original_main.main.assert_not_called()

    def test_passes_no_additional_arguments_to_zap_when_none_provided(self):
        ScanScriptWrapper(self.original_main, self.config, self.logger, self.target_website, self.system).run()

        self.original_main.main.assert_called_with([
            '-t', 'http://website',
            '-J', 'gl-dast-report.json'])

    def test_should_continue_scanning_even_if_target_website_is_not_available(self):
        site_check = ToObject(is_available=MagicMock(return_value=False),
                              unavailable_reason=MagicMock(return_value='a connect error'),
                              is_safe_to_scan=MagicMock(return_value=(True, None)))

        target_website = ToObject(address=MagicMock(return_value="http://website"),
                                  is_configured=MagicMock(return_value=True),
                                  check_site_is_available=MagicMock(return_value=site_check))

        ScanScriptWrapper(self.original_main, self.config, self.logger, target_website, self.system).run()

        self.system.exit.assert_not_called()
        self.original_main.main.assert_called_once()

    def test_should_pass_zap_arguments_to_zap(self):
        config = ToConfig(
            target="http://website",
            zap_config_file='config.file',
            zap_config_url='config.url',
            zap_gen_file='gen.file',
            zap_mins='10',
            zap_report_html='report.html',
            zap_report_md='report.md',
            zap_report_xml='report.xml',
            zap_include_alpha=True,
            zap_debug=True,
            zap_port='6001',
            zap_delay_in_seconds='50',
            zap_default_info=True,
            zap_no_fail_on_warn=True,
            zap_use_ajax_spider=True,
            zap_min_level='INFO',
            zap_context_file='context.file',
            zap_progress_file='progress.file',
            zap_short_format=True,
            zap_mins_to_wait='1',
            zap_other_options='a=b c=d')

        ScanScriptWrapper(self.original_main, config, self.logger, self.target_website, self.system).run()

        self.original_main.main.assert_called_with([
            '-t', 'http://website',
            '-J', 'gl-dast-report.json',
            '-c', 'config.file',
            '-u', 'config.url',
            '-g', 'gen.file',
            '-m', '10',
            '-r', 'report.html',
            '-w', 'report.md',
            '-x', 'report.xml',
            '-a',
            '-d',
            '-P', '6001',
            '-D', '50',
            '-i',
            '-I',
            '-j',
            '-l', 'INFO',
            '-n', 'context.file',
            '-p', 'progress.file',
            '-s',
            '-T', '1',
            '-z', 'a=b c=d'])
