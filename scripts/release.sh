#!/bin/sh

set -e

printf "\n\n######### Initializing environment variables #########\n"

if [ -z "$GITLAB_API_TOKEN" ]; then
  printf "Aborting, environment variable GITLAB_API_TOKEN must contain a GitLab private token with access to this project.\n"
  exit 1
fi

# required file paths
WORKING_DIRECTORY="$(dirname "$(realpath "$0")")"
FILE_CONTAINING_IMAGE_NAME=$(realpath "$WORKING_DIRECTORY/../built_image.txt")
FILE_CONTAINING_SUPPORTED_GITLAB_VERSION=$(realpath "$WORKING_DIRECTORY/../supported-gitlab-runner.txt")
CHANGELOG_FILE=$(realpath "$WORKING_DIRECTORY/../CHANGELOG.md")

if ! [ -f "$FILE_CONTAINING_IMAGE_NAME" ]; then
  printf "Aborting, unable to determine previously built/tested DAST image as file containing name doesn't exist: '%s'.\n" "$FILE_CONTAINING_IMAGE_NAME"
  exit 1
fi

if ! [ -f "$CHANGELOG_FILE" ]; then
  printf "Aborting, unable to determine DAST version as the changelog file doesn't exist: '%s'.\n" "$CHANGELOG_FILE"
  exit 1
fi

if ! [ -f "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION" ]; then
  printf "Aborting, unable to determine supported GitLab version as the file containing the version doesn't exist: '%s'.\n" "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION"
  exit 1
fi

VERSION_PATTERN='^## \(v[0-9]*\.[0-9]*\.[0-9]*\)'
DAST_IMAGE=$(cat "$FILE_CONTAINING_IMAGE_NAME")
GITLAB_RUNNER_VERSION=$(cat "$FILE_CONTAINING_SUPPORTED_GITLAB_VERSION")
VERSION=$(sed -n "s/$VERSION_PATTERN/\\1/p" "$CHANGELOG_FILE" | sed -n '1,1p;1q')
MAJOR_MINOR=$(echo "$VERSION" | awk -F '.' '{print $1"."$2}')
MAJOR=$(echo "$VERSION" | awk -F '.' '{print $1}')

# extract the latest version description from the CHANGELOG
CHANGELOG_DESCRIPTION_START=$(sed -n "/$VERSION_PATTERN/=" "$CHANGELOG_FILE" | sed -n '1,1p;1q' | awk '{print $0 + 1}')
CHANGELOG_DESCRIPTION_END=$(sed -n "/$VERSION_PATTERN/=" "$CHANGELOG_FILE" | sed -n '2,2p;2q' | awk '{print $0 - 2}')
CHANGELOG_DESCRIPTION=$(sed -n "${CHANGELOG_DESCRIPTION_START},${CHANGELOG_DESCRIPTION_END}p;${CHANGELOG_DESCRIPTION_END}q" "$CHANGELOG_FILE")

printf "\n\n######### Determined release version of DAST to be %s, compatible with GitLab %s #########\n" "$VERSION" "$GITLAB_RUNNER_VERSION"
printf "\n\n######### Verifying DAST %s is not already present #########\n" "$VERSION"

# verify a release with this version does not already exist
if curl --silent --fail --show-error --header "private-token:$GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags/$VERSION"; then
  printf "\nAborting, tag %s already exists. If this is not expected, please remove the tag and try again.\n" "$VERSION"
  exit 1
fi

printf "\n\n######### Authenticating with Docker #########\n"
docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

printf "\n\n######### Retrieving DAST Docker image %s so it can be tagged #########\n" "$DAST_IMAGE"
docker pull "$DAST_IMAGE"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$VERSION"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$VERSION"
docker push "$CI_REGISTRY_IMAGE:$VERSION"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$MAJOR_MINOR"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$MAJOR_MINOR"
docker push "$CI_REGISTRY_IMAGE:$MAJOR_MINOR"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$MAJOR"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$MAJOR"
docker push "$CI_REGISTRY_IMAGE:$MAJOR"

printf "\n\n######### Creating Docker tag %s:latest #########\n" "$CI_REGISTRY_IMAGE"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:latest"
docker push "$CI_REGISTRY_IMAGE:latest"

printf "\n\n######### Creating Docker tag %s:%s #########\n" "$CI_REGISTRY_IMAGE" "$GITLAB_RUNNER_VERSION"
docker tag "$DAST_IMAGE" "$CI_REGISTRY_IMAGE:$GITLAB_RUNNER_VERSION"
docker push "$CI_REGISTRY_IMAGE:$GITLAB_RUNNER_VERSION"

# the following magical sed string replaces new lines with the string \n: https://stackoverflow.com/questions/1251999/how-can-i-replace-a-newline-n-using-sed/1252010#7697604
RELEASE_DATA=$(echo "{\"tag_name\":\"$VERSION\",\"description\":\"$CHANGELOG_DESCRIPTION\"}" | sed ':a;N;$!ba;s/\n/\\n/g')

printf "\n\n######### Tagging Git SHA %s with %s #########\n" "$CI_COMMIT_SHA" "$VERSION"
curl --silent --fail --show-error --request POST --header "PRIVATE-TOKEN:$GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$VERSION&ref=$CI_COMMIT_SHA"

printf "\n\n######### Creating GitLab release from Git tag %s #########\n" "$VERSION"
curl --silent --fail --show-error --request POST --header "PRIVATE-TOKEN:$GITLAB_API_TOKEN" --header 'Content-Type:application/json' --data "$RELEASE_DATA" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases"
