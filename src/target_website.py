import requests
from time import sleep
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from target_website_availability_check import TargetWebsiteAvailabilityCheck


class TargetWebsite:
    def __init__(self, logger, config):
        self.logger = logger
        self.config = config
        self.attempted_availability_check = False
        self.most_recent_connect_error = None
        self.most_recent_response = None
        self.reason_unsafe_to_scan = ''

    def address(self):
        return self.config.target

    def is_configured(self):
        return self.address() is not None

    def check_site_is_available(self, max_attempts=20, request_timeout=5, retry_delay=3):
        # thank you, we know that certificates should ideally be validated
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

        check = TargetWebsiteAvailabilityCheck(False, self.config)

        for attempt in range(0, max_attempts):
            check = self._check_site(request_timeout)

            if check.is_available():
                break

            if attempt < max_attempts - 1:
                sleep(retry_delay)

        return check

    def _check_site(self, request_timeout):
        try:
            self.logger.info('requesting access to {}'.format(self.address()))
            response = requests.get(self.address(), timeout=request_timeout, verify=False)
            response.raise_for_status()
            return TargetWebsiteAvailabilityCheck(True, self.config, response=response)

        except requests.exceptions.RequestException as e:
            return TargetWebsiteAvailabilityCheck(False, self.config, unavailable_reason=e)
