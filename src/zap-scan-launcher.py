#!/usr/bin/env python

from dependencies import config, FullScan, BaselineScan

if __name__ == "__main__":
    if config.full_scan:
        FullScan.run()
    else:
        BaselineScan.run()
